# IsWonky

Globally accepted sudo code for determining if the parameter(s) passed are weird, whacked out, messed up, not working for no definable reason.


### Entity Analysis
~~~~
SET x = 'nicolas cage'
SET sResult = IsWonky(x);
PRINT sResult

Result
---------------
Yes, Nicolas Cage is Wonky
~~~~

### Media Analysis
~~~~
SET x = 'teletubbies'
SET sResult = IsWonky(x);
PRINT sResult

Result
---------------
Duh, Teletubbies are wonky AF.
~~~~

### Code Analysis
~~~~
SET x = "
if a = 0 then
a = 0
end if
"
PRINT = IsWonky(x);

Result
---------------
a = Wonky
~~~~